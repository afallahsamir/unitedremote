# unitedRemote

Back-end:

* Technologies: NodeJS (Loopback Framework V3), MongoDB
* How to use it: Run 'node .' in back-end directory

Front-end:

* Technologies: VueJS2, Lodash, Bootstrap4
* How to use it: Run it on the browser


Samir AFALLAH
afallahsamir@gmail.com
Senior Software Engineer
